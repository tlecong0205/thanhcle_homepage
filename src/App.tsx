import Container from "react-bootstrap/Container";
import Abstract from "./Abstract";
import Publications from "./Publications";
import WorkExperience from "./Work";
import Education from "./Education";
import Research from "./Research";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInbox,
  faFile,
  faUser,
  faIdBadge,
} from "@fortawesome/free-solid-svg-icons";
import {
  faResearchgate,
  faFacebook,
  faGithub,
  faLinkedin,
} from "@fortawesome/free-brands-svg-icons";
import Button from "react-bootstrap/Button";
import "./academicons/css/academicons.css";

function Contact() {
  return (
    <>
      <h3>
        <FontAwesomeIcon icon={faInbox} /> Contact
      </h3>
      <h4>Work: tlecong{`[at]`}smu.edu.sg</h4>
      <h4>Personal: thanhcls1316{`[at]`}gmail.com</h4>
      <Button href="https://www.facebosok.com/thanh.toanlamson" variant="outline-success">
        <FontAwesomeIcon icon={faFacebook} /> Facebook
      </Button>{" "}
      <Button href="https://github.com/thanhdeku" variant="outline-warning">
        <FontAwesomeIcon icon={faGithub} /> Github
      </Button>{" "}
      <Button href="https://linkedin.com/in/handk" variant="outline-info">
        <FontAwesomeIcon icon={faLinkedin} /> LinkedIn
      </Button>{" "}
    </>
  );
}

function BibliographicProfiles() {
  return (
    <>
      <h3>
        <FontAwesomeIcon icon={faIdBadge} /> Bibliographic Profiles{" "}
      </h3>
      <Button href="https://orcid.org/my-orcid?orcid=0000-0002-9566-324X" variant="outline-success">
        <i className="ai ai-orcid" /> ORCID
      </Button>{" "}
      <Button href="https://scholar.google.com/citations?user=iaQgZHYAAAAJ&hl=en" variant="outline-warning">
        <i className="ai ai-google-scholar" /> Google Scholar
      </Button>{" "}
      <Button href="https://www.researchgate.net/profile/Le-Thanh-27" variant="outline-info">
        <FontAwesomeIcon icon={faResearchgate} /> ResearchGate
      </Button>{" "}
    </>
  );
}

function CV() {
  return (
    <>
      <h3>
        <FontAwesomeIcon icon={faUser} /> Curriculum Vitae
      </h3>
      <Button
        href="https://drive.google.com/file/d/1Kvi13gKjdLT7ZI2JsY2GpwKwFw-AazUS/view?usp=sharing"
        variant="outline-danger"
      >
        <FontAwesomeIcon icon={faFile} /> View my CV
      </Button>
    </>
  );
}

function App() {
  return (
    <>
      <Abstract />
      <Container>
        <hr />
        <Contact />
        <hr />
        <CV />
        <hr />
        <BibliographicProfiles />
        <hr />
        <Publications />
        <hr />
        <Education />
        <hr />
        <WorkExperience />
        {/* <hr />
        <TeachingExperience />
        <hr /> */}
        {/* <Service /> */}
        <footer style={{ marginTop: "5rem", marginBottom: "3rem" }}>
          <hr />
          <p className="text-center"> This template is created and published by 
            &copy; 2021 DongGyun Han. Please refer to his <a href="https://github.com/handk85/"> Github </a> for <a href="https://github.com/handk85/react-homepage">
              source code of this homepage
            </a>
            &nbsp;.
          </p>
        </footer>
      </Container>
    </>
  );
}

export default App;
