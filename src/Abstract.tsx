import Container from "react-bootstrap/Container";
import Figure from "react-bootstrap/Figure";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function Abstract() {
  return (
    <Container style={{ marginTop: "2rem" }}>
      <Row>
        <Col md="auto">
          <Figure>
            <Figure.Image width={250} alt="Le-Cong Thanh" src="./profile.jpg" />
          </Figure>
        </Col>
        <Col>
          <h2>Le-Cong Thanh</h2>
          <h4>Research Engineer, Singapore Management University</h4>
          Le-Cong Thanh is a research engineer at{" "}
          <a href="https://www.smu.edu.sg">
            Singapore Management University (SMU)
          </a>
          . Previously, he obtained B.E. in Information Technology from Talented Engineering Program (Top 1% in Entrance Examination) of <a href="https://en.hust.edu.vn/">
           Hanoi University of Science and Technology
          </a>, where he worked closely with <a href="https://users.soict.hust.edu.vn/thanghq/"> Prof. Huynh Quyet Thang </a> in the area of Software Engineering (SE). He is mainly working on the intersection between software engineering and
          artificial intelligence under the supervision of{" "}
          <a href="http://www.mysmu.edu/faculty/davidlo/">Prof. David Lo</a>.
          His main research interests are automated software debugging, vulnerability detection and property/invariant inference (for neural network).
        </Col>
      </Row>
    </Container>
  );
}

export default Abstract;
