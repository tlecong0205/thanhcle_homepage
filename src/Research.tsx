import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSuitcase } from "@fortawesome/free-solid-svg-icons";
import { ReactNode, useEffect, useState } from "react";
import { load } from "js-yaml";
import { Spinner } from "react-bootstrap";
import { ResearchItem } from "./Types";

function Research() {
  const [research, setResearch] = useState<ResearchItem[]>();
  useEffect(() => {
    fetch("./data/research.yaml").then(async (response) => {
      const text = await response.text();
      const data = (await load(text)) as ResearchItem[];
      setResearch(data);
    });
  }, []);

  return (
    <>
      <h3>
        <FontAwesomeIcon icon={faSuitcase} /> Ongoing Projects
      </h3>
      {!research ? (
        <Spinner animation="border" />
      ) : (
        <ul>
          {research.map((item) => {
            return (
                <li>
                {item.name}
                {item.supervisor && (
                  <ul>
                    <li>
                      {item.collaborator.length < 2
                        ? "Collaborator"
                        : "Collaborators"}
                      :{" "}
                      {item.collaborator
                        .map<ReactNode>((item) => {
                          return item.homepage ? (
                            <span>
                              <a href={item.homepage}>{item.name}</a>{" "}
                              {item.note && `(${item.note})`}
                            </span>
                          ) : (
                            <span>
                              {item.name} {item.note && `(${item.note})`}
                            </span>
                          );
                        })
                        .reduce((prev, curr) => [prev, ", ", curr])}
                    </li>
                  </ul>
                )}
                {item.supervisor && (
                  <ul>
                    <li>
                      {item.supervisor.length < 2
                        ? "Supervisor"
                        : "Supervisors"}
                      :{" "}
                      {item.supervisor
                        .map<ReactNode>((item) => {
                          return item.homepage ? (
                            <span>
                              <a href={item.homepage}>{item.name}</a>{" "}
                              {item.note && `(${item.note})`}
                            </span>
                          ) : (
                            <span>
                              {item.name} {item.note && `(${item.note})`}
                            </span>
                          );
                        })
                        .reduce((prev, curr) => [prev, ", ", curr])}
                    </li>
                  </ul>
                )}
              </li>
            );
          })}
        </ul>
      )}
    </>
  );
}

export default Research;
